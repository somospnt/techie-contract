package com.somospnt.contractproducer.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cliente {
    
    private Long id;
    private String nombre;
    
}
