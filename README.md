# techie-contract

## Descripcion ## 
Spring contract nos sirve para testear la compatibilidad entre apis desarrolladas por nosotros, por ejemplo en microservicios.
Se defines dos componentes para poder crear estos contratos.
* Por un lado el "producer" que sera el servicio que expone el api y por ende el que tendra la definicion del contrato.
* Por otro lado el "consumer" que sera el servicio que consume al "producer" y por ende quien tendra que validar su implementacion contra el contrato establecido en el "producer".

## Pasos a seguir ##

### Producer ###
Primero se debe crear un contrato en el producer, en este se define como sera el api expuesta por el servicio y cuales son las distintas respuestas que puede dar.
Esto nos autogenerara un test, el cual correra contra si mismo para comprobar que el producer esta cumpliendo el contrato establecido.

* Agregar las siguiente dependencia en el pom
```xml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-contract-verifier</artifactId>
        <version>2.2.3.RELEASE</version>
        <scope>test</scope>
    </dependency>
```
* Agregar el siguiente plugin remplazando el placeholder por el paquete raiz de test
```xml
    <plugin>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-contract-maven-plugin</artifactId>
        <version>2.2.3.RELEASE</version>
        <extensions>true</extensions>
        <configuration>
            <baseClassForTests>
                {paquete raiz de test}
            </baseClassForTests>
            <testFramework>JUNIT5</testFramework> 
        </configuration>
    </plugin>
```
* En el paquete raiz de test agregar el siguiente codigo
```java
@DirtiesContext
@AutoConfigureMessageVerifier
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
```
-----------------------------------------
```java
@BeforeEach
public void setup() {
    StandaloneMockMvcBuilder standaloneMockMvcBuilder
            = MockMvcBuilders.standaloneSetup({instancias injectadas de todos los controller});
    RestAssuredMockMvc.standaloneSetup(standaloneMockMvcBuilder);
}
```
* Y por ultimo la definicion de los contratos que se ubicaran en la carpeta src\test\resources\contracts\package\
Estos contratos pueden escribirse en varios lenguajes como ser groovy, yamel y otros.
Mas info en https://cloud.spring.io/spring-cloud-static/spring-cloud-contract/2.1.0.RELEASE/single/spring-cloud-contract.html#contract-dsl

### Consumer ###
Una ves creado el contrato en el producer debemos crear el test para verificar que estamos consumiendo el contrato de manera correcta y que si este cambia habra un test que nos avise.
* Agregar las siguiente dependencia en el pom
```xml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-contract-stub-runner</artifactId>
        <version>2.2.3.RELEASE</version>
        <scope>test</scope>
    </dependency>
```
* Luego en el test en cual comprobaremos el consumo del api del producer debemos ingresar las sugientes anotaciones
```java
@AutoConfigureStubRunner(
        stubsMode = StubRunnerProperties.StubsMode.LOCAL,
        ids = "{groupId}:{artifactId}:{version}:stubs:{puerto}")
```
De esta manera cuando corramos el test spring contrat buscara dentro del repositorio local de maven el jar generado por spring contract en el producer y lo correra de manera local en el puerto indicado.
Esto se puede modificar de varias maneras, entre ellas para que lo busque en un repo remoto, o indicandole la ultima version sin necesidad de especificar una en especial.


