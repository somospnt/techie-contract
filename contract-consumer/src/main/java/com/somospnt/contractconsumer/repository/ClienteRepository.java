package com.somospnt.contractconsumer.repository;

import com.somospnt.contractconsumer.domain.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class ClienteRepository {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${com.somospnt.contractconsumer.cliente-api.url}")
    private String urlClienteApi;

    public Cliente buscarPorId(Long id) {
        Cliente cliente = restTemplate.getForObject(urlClienteApi + id, Cliente.class);
        return cliente;
    }

}
