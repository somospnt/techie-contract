package com.somospnt.contractconsumer.web.controller.rest;

import com.somospnt.contractconsumer.domain.Cliente;
import com.somospnt.contractconsumer.repository.ClienteRepository;
import com.somospnt.contractconsumer.repository.CreditoRepository;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreditoController {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private CreditoRepository creditoRepository;

    @GetMapping("/credito")
    public BigDecimal buscarCreditoDisponible(@RequestParam("cliente-id") Long clienteId) {
        Cliente cliente = clienteRepository.buscarPorId(clienteId);
        BigDecimal creditoDisponible = creditoRepository.buscarCreditoPorClienteId(cliente.getId());
        return creditoDisponible;
    }
}
